import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'dashboard',
    children: [
      {
        path: 'forecast/:id',
        loadChildren:
          'src/app/forecast/forecast.module#ForecastModule',
      },
      {
        path: 'forecast',
        redirectTo: '/dashboard',
        pathMatch: 'full',
      },
      {
        path: '',
        component: DashboardComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class DashboardRoutingModule {}
