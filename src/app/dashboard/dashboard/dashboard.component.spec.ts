import { BreakpointObserver } from '@angular/cdk/layout';
import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { dummyWeather } from '@core/dummy-weather';
import { WeatherService } from '@core/weather.service';
import { of } from 'rxjs';
import { MaterialModule } from 'src/app/material/material.module';
import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const weatherService: Partial<WeatherService> = {
    getCitiesWeather: () => of([dummyWeather]),
  };
  const breakpointObserver: Partial<BreakpointObserver> = {
    observe: () => of(null),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [
        NoopAnimationsModule,
        MaterialModule,
        RouterModule.forRoot([]),
      ],
      providers: [
        { provide: WeatherService, useValue: weatherService },
        { provide: BreakpointObserver, useValue: breakpointObserver },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
