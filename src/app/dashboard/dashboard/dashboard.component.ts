import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { WeatherService } from '@core/weather.service';
import { CityWeather } from '@shared/interfaces/weather.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  layoutColumns: Observable<number>;
  citiesWeather: Observable<CityWeather[]>;

  constructor(
    private weatherService: WeatherService,
    private breakpointObserver: BreakpointObserver,
  ) {
    this.citiesWeather = this.weatherService.getCitiesWeather();
    this.layoutColumns = this.breakpointObserver
      .observe(Breakpoints.Handset)
      .pipe(map(({ matches }) => (matches ? 1 : 2)));
  }
}
