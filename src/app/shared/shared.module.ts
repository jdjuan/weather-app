import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
  declarations: [NotFoundComponent, ToolbarComponent],
  imports: [CommonModule, MaterialModule],
  exports: [ToolbarComponent],
})
export class SharedModule {}
