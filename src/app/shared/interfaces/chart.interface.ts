export interface Chart {
  name: string;
  series: { name: string; value: number }[];
}
