import { formatDate } from '@angular/common';
import { Component, Inject, LOCALE_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '@core/weather.service';
import { Chart } from '@shared/interfaces/chart.interface';
import {
  Forecast,
  List,
} from '@shared/interfaces/forecast.interface';
import * as shape from 'd3-shape';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
})
export class ForecastComponent {
  forecast: Observable<Forecast>;
  curve: shape.CurveFactory = shape.curveCatmullRom;
  yScaleMax: number;
  yScaleMin: number;
  data: Chart[];

  constructor(
    private router: ActivatedRoute,
    private weatherService: WeatherService,
    @Inject(LOCALE_ID) private locale: string,
  ) {
    this.router.params
      .pipe(switchMap(this.weatherService.getForecastByCityId))
      .subscribe(this.populateData);
  }

  populateData = (forecast: Forecast) => {
    this.determineMaxMinChartValues(forecast.list);
    // Use the first 8 forecasts to ensure chart readability
    forecast.list = forecast.list.slice(0, 8);
    this.data = this.formatData(forecast);
    // tslint:disable-next-line: semicolon
  };

  // Used to define max/min values in the chart so lines are displayed vertically centered
  determineMaxMinChartValues(weather: List[]) {
    const chartDelta = 3;
    const temperatures = weather.map((item) => item.main.temp);
    this.yScaleMax = Math.max(...temperatures) + chartDelta;
    this.yScaleMin = Math.min(...temperatures) - chartDelta;
  }

  formatData(forecast: Forecast): Chart[] {
    const series = forecast.list.map((weather: List) => {
      // tslint:disable-next-line: no-shadowed-variable
      const name = formatDate(weather.dt_txt, 'h a', this.locale);
      const value = weather.main.temp;
      return { name, value };
    });
    const name = forecast.city.name;
    return [{ name, series }];
  }
}
