import { Forecast } from '@shared/interfaces/forecast.interface';

export const dummyForecast: Forecast = {
  cod: '200',
  message: 0.0048,
  cnt: 40,
  list: [
    {
      dt: 1551711600,
      main: {
        temp: 293.76,
        temp_min: 291.572,
        temp_max: 293.76,
        pressure: 1011.91,
        sea_level: 1011.91,
        grnd_level: 825.03,
        humidity: 92,
        temp_kf: 2.19,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 68,
      },
      wind: {
        speed: 0.35,
        deg: 51.0009,
      },
      rain: {
        '3h': 0.03,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-04 15:00:00',
    },
    {
      dt: 1551722400,
      main: {
        temp: 296.05,
        temp_min: 294.412,
        temp_max: 296.05,
        pressure: 1008.73,
        sea_level: 1008.73,
        grnd_level: 823.08,
        humidity: 76,
        temp_kf: 1.64,
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d',
        },
      ],
      clouds: {
        all: 68,
      },
      wind: {
        speed: 0.86,
        deg: 96.5005,
      },
      rain: {},
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-04 18:00:00',
    },
    {
      dt: 1551733200,
      main: {
        temp: 296.06,
        temp_min: 294.967,
        temp_max: 296.06,
        pressure: 1006.25,
        sea_level: 1006.25,
        grnd_level: 821.41,
        humidity: 71,
        temp_kf: 1.09,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 44,
      },
      wind: {
        speed: 0.91,
        deg: 92.0028,
      },
      rain: {
        '3h': 0.035,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-04 21:00:00',
    },
    {
      dt: 1551744000,
      main: {
        temp: 290.24,
        temp_min: 289.692,
        temp_max: 290.24,
        pressure: 1007.8,
        sea_level: 1007.8,
        grnd_level: 822.13,
        humidity: 100,
        temp_kf: 0.55,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 36,
      },
      wind: {
        speed: 0.76,
        deg: 10.5042,
      },
      rain: {
        '3h': 3.23,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-05 00:00:00',
    },
    {
      dt: 1551754800,
      main: {
        temp: 289.443,
        temp_min: 289.443,
        temp_max: 289.443,
        pressure: 1010.86,
        sea_level: 1010.86,
        grnd_level: 824.11,
        humidity: 97,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 76,
      },
      wind: {
        speed: 0.77,
        deg: 79.5001,
      },
      rain: {
        '3h': 0.05,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-05 03:00:00',
    },
    {
      dt: 1551765600,
      main: {
        temp: 289.051,
        temp_min: 289.051,
        temp_max: 289.051,
        pressure: 1010.33,
        sea_level: 1010.33,
        grnd_level: 823.24,
        humidity: 97,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 76,
      },
      wind: {
        speed: 0.82,
        deg: 67.5024,
      },
      rain: {
        '3h': 0.125,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-05 06:00:00',
    },
    {
      dt: 1551776400,
      main: {
        temp: 288.428,
        temp_min: 288.428,
        temp_max: 288.428,
        pressure: 1009.27,
        sea_level: 1009.27,
        grnd_level: 822.43,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 92,
      },
      wind: {
        speed: 0.82,
        deg: 47.5077,
      },
      rain: {
        '3h': 0.39,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-05 09:00:00',
    },
    {
      dt: 1551787200,
      main: {
        temp: 288.371,
        temp_min: 288.371,
        temp_max: 288.371,
        pressure: 1010.68,
        sea_level: 1010.68,
        grnd_level: 823.37,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 88,
      },
      wind: {
        speed: 0.71,
        deg: 45.5026,
      },
      rain: {
        '3h': 0.23,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-05 12:00:00',
    },
    {
      dt: 1551798000,
      main: {
        temp: 292.041,
        temp_min: 292.041,
        temp_max: 292.041,
        pressure: 1011.19,
        sea_level: 1011.19,
        grnd_level: 824.5,
        humidity: 88,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 36,
      },
      wind: {
        speed: 0.98,
        deg: 99.0009,
      },
      rain: {
        '3h': 0.02,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-05 15:00:00',
    },
    {
      dt: 1551808800,
      main: {
        temp: 296.538,
        temp_min: 296.538,
        temp_max: 296.538,
        pressure: 1007.7,
        sea_level: 1007.7,
        grnd_level: 822.23,
        humidity: 67,
        temp_kf: 0,
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02d',
        },
      ],
      clouds: {
        all: 24,
      },
      wind: {
        speed: 0.87,
        deg: 167.501,
      },
      rain: {},
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-05 18:00:00',
    },
    {
      dt: 1551819600,
      main: {
        temp: 293.842,
        temp_min: 293.842,
        temp_max: 293.842,
        pressure: 1006,
        sea_level: 1006,
        grnd_level: 821.25,
        humidity: 86,
        temp_kf: 0,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 20,
      },
      wind: {
        speed: 0.86,
        deg: 89.5001,
      },
      rain: {
        '3h': 3.87,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-05 21:00:00',
    },
    {
      dt: 1551830400,
      main: {
        temp: 291.437,
        temp_min: 291.437,
        temp_max: 291.437,
        pressure: 1007.66,
        sea_level: 1007.66,
        grnd_level: 822.14,
        humidity: 92,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 48,
      },
      wind: {
        speed: 0.71,
        deg: 55.5027,
      },
      rain: {
        '3h': 0.1,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-06 00:00:00',
    },
    {
      dt: 1551841200,
      main: {
        temp: 290.512,
        temp_min: 290.512,
        temp_max: 290.512,
        pressure: 1010.5,
        sea_level: 1010.5,
        grnd_level: 823.88,
        humidity: 95,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 44,
      },
      wind: {
        speed: 0.86,
        deg: 79.0024,
      },
      rain: {
        '3h': 0.26,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-06 03:00:00',
    },
    {
      dt: 1551852000,
      main: {
        temp: 289.331,
        temp_min: 289.331,
        temp_max: 289.331,
        pressure: 1010.14,
        sea_level: 1010.14,
        grnd_level: 823.41,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 100,
      },
      wind: {
        speed: 0.86,
        deg: 67.5054,
      },
      rain: {
        '3h': 1.24,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-06 06:00:00',
    },
    {
      dt: 1551862800,
      main: {
        temp: 289.094,
        temp_min: 289.094,
        temp_max: 289.094,
        pressure: 1009.45,
        sea_level: 1009.45,
        grnd_level: 822.49,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 56,
      },
      wind: {
        speed: 0.62,
        deg: 58.0035,
      },
      rain: {
        '3h': 0.029999999999999,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-06 09:00:00',
    },
    {
      dt: 1551873600,
      main: {
        temp: 289.291,
        temp_min: 289.291,
        temp_max: 289.291,
        pressure: 1011.19,
        sea_level: 1011.19,
        grnd_level: 823.83,
        humidity: 97,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 88,
      },
      wind: {
        speed: 0.92,
        deg: 82.0004,
      },
      rain: {
        '3h': 0.12,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-06 12:00:00',
    },
    {
      dt: 1551884400,
      main: {
        temp: 293.075,
        temp_min: 293.075,
        temp_max: 293.075,
        pressure: 1011.64,
        sea_level: 1011.64,
        grnd_level: 824.86,
        humidity: 83,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 20,
      },
      wind: {
        speed: 0.51,
        deg: 194.5,
      },
      rain: {
        '3h': 0.039999999999999,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-06 15:00:00',
    },
    {
      dt: 1551895200,
      main: {
        temp: 295.661,
        temp_min: 295.661,
        temp_max: 295.661,
        pressure: 1008.41,
        sea_level: 1008.41,
        grnd_level: 822.79,
        humidity: 76,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 24,
      },
      wind: {
        speed: 0.76,
        deg: 130.504,
      },
      rain: {
        '3h': 0.33,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-06 18:00:00',
    },
    {
      dt: 1551906000,
      main: {
        temp: 292.638,
        temp_min: 292.638,
        temp_max: 292.638,
        pressure: 1006.91,
        sea_level: 1006.91,
        grnd_level: 821.85,
        humidity: 93,
        temp_kf: 0,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 32,
      },
      wind: {
        speed: 0.46,
        deg: 41.5021,
      },
      rain: {
        '3h': 9.39,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-06 21:00:00',
    },
    {
      dt: 1551916800,
      main: {
        temp: 290.188,
        temp_min: 290.188,
        temp_max: 290.188,
        pressure: 1008.28,
        sea_level: 1008.28,
        grnd_level: 822.73,
        humidity: 98,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 44,
      },
      wind: {
        speed: 0.77,
        deg: 323.502,
      },
      rain: {
        '3h': 1.89,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-07 00:00:00',
    },
    {
      dt: 1551927600,
      main: {
        temp: 289.089,
        temp_min: 289.089,
        temp_max: 289.089,
        pressure: 1010.61,
        sea_level: 1010.61,
        grnd_level: 824.03,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 64,
      },
      wind: {
        speed: 0.77,
        deg: 85.5034,
      },
      rain: {
        '3h': 0.010000000000002,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-07 03:00:00',
    },
    {
      dt: 1551938400,
      main: {
        temp: 289.121,
        temp_min: 289.121,
        temp_max: 289.121,
        pressure: 1010.36,
        sea_level: 1010.36,
        grnd_level: 823.56,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 76,
      },
      wind: {
        speed: 0.87,
        deg: 71.0006,
      },
      rain: {
        '3h': 1.12,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-07 06:00:00',
    },
    {
      dt: 1551949200,
      main: {
        temp: 288.346,
        temp_min: 288.346,
        temp_max: 288.346,
        pressure: 1009.64,
        sea_level: 1009.64,
        grnd_level: 823,
        humidity: 98,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 92,
      },
      wind: {
        speed: 0.62,
        deg: 50.0005,
      },
      rain: {
        '3h': 2.74,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-07 09:00:00',
    },
    {
      dt: 1551960000,
      main: {
        temp: 288.521,
        temp_min: 288.521,
        temp_max: 288.521,
        pressure: 1010.89,
        sea_level: 1010.89,
        grnd_level: 823.62,
        humidity: 99,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 68,
      },
      wind: {
        speed: 0.86,
        deg: 74.5005,
      },
      rain: {
        '3h': 0.11,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-07 12:00:00',
    },
    {
      dt: 1551970800,
      main: {
        temp: 293.198,
        temp_min: 293.198,
        temp_max: 293.198,
        pressure: 1011.24,
        sea_level: 1011.24,
        grnd_level: 824.59,
        humidity: 83,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 8,
      },
      wind: {
        speed: 0.86,
        deg: 68.0024,
      },
      rain: {
        '3h': 0.010000000000002,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-07 15:00:00',
    },
    {
      dt: 1551981600,
      main: {
        temp: 296.459,
        temp_min: 296.459,
        temp_max: 296.459,
        pressure: 1007.44,
        sea_level: 1007.44,
        grnd_level: 822.09,
        humidity: 68,
        temp_kf: 0,
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d',
        },
      ],
      clouds: {
        all: 76,
      },
      wind: {
        speed: 0.46,
        deg: 118.001,
      },
      rain: {},
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-07 18:00:00',
    },
    {
      dt: 1551992400,
      main: {
        temp: 295.954,
        temp_min: 295.954,
        temp_max: 295.954,
        pressure: 1004.66,
        sea_level: 1004.66,
        grnd_level: 820.3,
        humidity: 66,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 64,
      },
      wind: {
        speed: 0.82,
        deg: 78.503,
      },
      rain: {
        '3h': 0.079999999999998,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-07 21:00:00',
    },
    {
      dt: 1552003200,
      main: {
        temp: 290.701,
        temp_min: 290.701,
        temp_max: 290.701,
        pressure: 1006.12,
        sea_level: 1006.12,
        grnd_level: 821.15,
        humidity: 88,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 12,
      },
      wind: {
        speed: 0.56,
        deg: 349.001,
      },
      rain: {
        '3h': 1.5375,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-08 00:00:00',
    },
    {
      dt: 1552014000,
      main: {
        temp: 288.249,
        temp_min: 288.249,
        temp_max: 288.249,
        pressure: 1008.72,
        sea_level: 1008.72,
        grnd_level: 822.89,
        humidity: 97,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 20,
      },
      wind: {
        speed: 0.75,
        deg: 72.0028,
      },
      rain: {
        '3h': 0.6625,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-08 03:00:00',
    },
    {
      dt: 1552024800,
      main: {
        temp: 287.532,
        temp_min: 287.532,
        temp_max: 287.532,
        pressure: 1008.15,
        sea_level: 1008.15,
        grnd_level: 822.28,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02n',
        },
      ],
      clouds: {
        all: 12,
      },
      wind: {
        speed: 0.81,
        deg: 74.5043,
      },
      rain: {},
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-08 06:00:00',
    },
    {
      dt: 1552035600,
      main: {
        temp: 287.243,
        temp_min: 287.243,
        temp_max: 287.243,
        pressure: 1007.33,
        sea_level: 1007.33,
        grnd_level: 821.28,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 48,
      },
      wind: {
        speed: 0.81,
        deg: 88.0054,
      },
      rain: {
        '3h': 0.012500000000003,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-08 09:00:00',
    },
    {
      dt: 1552046400,
      main: {
        temp: 287.678,
        temp_min: 287.678,
        temp_max: 287.678,
        pressure: 1008.83,
        sea_level: 1008.83,
        grnd_level: 822.3,
        humidity: 100,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 48,
      },
      wind: {
        speed: 0.86,
        deg: 82.0009,
      },
      rain: {
        '3h': 0.049999999999997,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-08 12:00:00',
    },
    {
      dt: 1552057200,
      main: {
        temp: 293.831,
        temp_min: 293.831,
        temp_max: 293.831,
        pressure: 1009.29,
        sea_level: 1009.29,
        grnd_level: 823.54,
        humidity: 80,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 12,
      },
      wind: {
        speed: 1.06,
        deg: 110,
      },
      rain: {
        '3h': 0.037500000000001,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-08 15:00:00',
    },
    {
      dt: 1552068000,
      main: {
        temp: 298.122,
        temp_min: 298.122,
        temp_max: 298.122,
        pressure: 1005.55,
        sea_level: 1005.55,
        grnd_level: 821.1,
        humidity: 60,
        temp_kf: 0,
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02d',
        },
      ],
      clouds: {
        all: 20,
      },
      wind: {
        speed: 0.97,
        deg: 153.003,
      },
      rain: {},
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-08 18:00:00',
    },
    {
      dt: 1552078800,
      main: {
        temp: 296.412,
        temp_min: 296.412,
        temp_max: 296.412,
        pressure: 1003.45,
        sea_level: 1003.45,
        grnd_level: 819.59,
        humidity: 64,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 20,
      },
      wind: {
        speed: 0.67,
        deg: 77.0016,
      },
      rain: {
        '3h': 0.475,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-08 21:00:00',
    },
    {
      dt: 1552089600,
      main: {
        temp: 290.097,
        temp_min: 290.097,
        temp_max: 290.097,
        pressure: 1005.68,
        sea_level: 1005.68,
        grnd_level: 821.01,
        humidity: 94,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 24,
      },
      wind: {
        speed: 0.62,
        deg: 294.003,
      },
      rain: {
        '3h': 2.25,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-09 00:00:00',
    },
    {
      dt: 1552100400,
      main: {
        temp: 288.856,
        temp_min: 288.856,
        temp_max: 288.856,
        pressure: 1008.44,
        sea_level: 1008.44,
        grnd_level: 822.75,
        humidity: 97,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 8,
      },
      wind: {
        speed: 0.7,
        deg: 51.5125,
      },
      rain: {
        '3h': 0.125,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-09 03:00:00',
    },
    {
      dt: 1552111200,
      main: {
        temp: 288.668,
        temp_min: 288.668,
        temp_max: 288.668,
        pressure: 1008.05,
        sea_level: 1008.05,
        grnd_level: 822.03,
        humidity: 96,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 56,
      },
      wind: {
        speed: 0.82,
        deg: 74.0005,
      },
      rain: {
        '3h': 0.024999999999999,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-09 06:00:00',
    },
    {
      dt: 1552122000,
      main: {
        temp: 288.747,
        temp_min: 288.747,
        temp_max: 288.747,
        pressure: 1007.52,
        sea_level: 1007.52,
        grnd_level: 821.15,
        humidity: 97,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n',
        },
      ],
      clouds: {
        all: 88,
      },
      wind: {
        speed: 0.86,
        deg: 58.5052,
      },
      rain: {
        '3h': 0.024999999999999,
      },
      sys: {
        pod: 'n',
      },
      dt_txt: '2019-03-09 09:00:00',
    },
    {
      dt: 1552132800,
      main: {
        temp: 289.128,
        temp_min: 289.128,
        temp_max: 289.128,
        pressure: 1008.78,
        sea_level: 1008.78,
        grnd_level: 822.06,
        humidity: 98,
        temp_kf: 0,
      },
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d',
        },
      ],
      clouds: {
        all: 92,
      },
      wind: {
        speed: 0.86,
        deg: 62.0009,
      },
      rain: {
        '3h': 0.1,
      },
      sys: {
        pod: 'd',
      },
      dt_txt: '2019-03-09 12:00:00',
    },
  ],
  city: {
    id: 3674962,
    name: 'Medellín',
    coord: {
      lat: 6.2443,
      lon: -75.5736,
    },
    country: 'CO',
    population: 1999979,
  },
};
