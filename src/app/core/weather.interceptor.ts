import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class WeatherInteceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const params = req.params
      .append('APPID', environment.openWeatherMapKey)
      .append('units', 'metric'); // To return Celcius instead of Kelvin
    const authReq = req.clone({ params });
    return next.handle(authReq);
  }
}
