import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Forecast } from '../shared/interfaces/forecast.interface';
import { CityWeather } from '../shared/interfaces/weather.interface';

@Injectable()
export class WeatherService {
  constructor(private http: HttpClient) {}

  getCitiesWeather(): Observable<CityWeather[]> {
    return forkJoin(
      this.getCitiesIds().map((id: number) => {
        const url = `${environment.OWMWeatherBaseUrl}?id=${id}`;
        return this.http.get<CityWeather>(url);
      }),
    );
  }

  getForecastByCityId = ({ id }: Param): Observable<Forecast> => {
    const url = `${environment.OWMForecastBaseUrl}?id=${id}`;
    return this.http.get<Forecast>(url);
    // tslint:disable-next-line: semicolon
  };

  private getCitiesIds(): number[] {
    const graz = 2778067;
    const amsterdam = 2759794;
    const munich = 2867714;
    const stockholm = 2673730;
    const rome = 6539761;
    return [graz, amsterdam, munich, stockholm, rome];
  }
}

export interface Param {
  id: string;
}
