import { CommonModule } from '@angular/common';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { WeatherInteceptor } from './weather.interceptor';
import { WeatherService } from './weather.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [
    WeatherService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: WeatherInteceptor,
      multi: true,
    },
  ],
})
export class CoreModule {}
