export const dummyWeather = {
  coord: {
    lon: -75.57,
    lat: 6.24,
  },
  weather: [
    {
      id: 721,
      main: 'Haze',
      description: 'haze',
      icon: '50d',
    },
    {
      id: 741,
      main: 'Fog',
      description: 'fog',
      icon: '50d',
    },
  ],
  base: 'stations',
  main: {
    temp: 291.01,
    pressure: 1022,
    humidity: 64,
    temp_min: 287.15,
    temp_max: 295.15,
  },
  visibility: 10000,
  wind: {
    speed: 0.35,
    deg: 51.0009,
  },
  clouds: {
    all: 40,
  },
  dt: 1551708000,
  sys: {
    type: 1,
    id: 8577,
    message: 0.0042,
    country: 'CO',
    sunrise: 1551697996,
    sunset: 1551741271,
  },
  id: 3674962,
  name: 'Medellín',
  cod: 200,
};
