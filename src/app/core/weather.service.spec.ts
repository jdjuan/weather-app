import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { dummyWeather } from './dummy-weather';
import { WeatherService } from './weather.service';

describe('Weather Service', () => {
  let weatherService: WeatherService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      // Provide both the service-to-test and its (spy) dependency
      providers: [
        WeatherService,
        { provide: HttpClient, useValue: httpClientSpy },
      ],
    });
    // Inject both the service-to-test and its (spy) dependency
    weatherService = TestBed.get(WeatherService);
    httpClientSpy = TestBed.get(HttpClient);
  });

  it('should be created', () => {
    const service: WeatherService = TestBed.get(WeatherService);
    expect(service).toBeTruthy();
  });

  it('should return expected cities (HttpClient called five times)', () => {
    httpClientSpy.get.and.returnValue(of(dummyWeather));

    weatherService
      .getCitiesWeather()
      .subscribe(
        (citiesWeather) =>
          expect(citiesWeather[0]).toEqual(
            dummyWeather,
            'expected city',
          ),
        fail,
      );

    expect(httpClientSpy.get.calls.count()).toBe(5, 'five calls');
  });
});
