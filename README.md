# Weather App

## 1. Running Application

To execute this application run:

- `npm install`
- `npm start`

## 2. Technical Decisions

### 2.1 Dependencies used

- **Angular 7.2.0:** As requested in the exercise
- **Angular CLI:** As it is the most appropiate way to create an Angular application since it already has opinionated decisiones based on maintainability and scalability
- **Angular Material:** As it has a library of battle tested components which already satisfy conditions such as: accessibility, responsiveness, and performance
- **Angular Router:** To guarantee specific routes for every city enabling users to share the weather in a specific city with the URL. Also, to enable lazy loading and preloading strategies.
- **Angular HTTP Interceptors:** To gracefully handle the addition of headers in the HTTP Request
- **Lazy Loading:** To improve performance of the application for the first load
- **CompoDocs:** To provide an interactive way to see the documentation
- **ngx-charts:** To provide d3.js-based charts

### 2.2 Routes

- **`/`** = Default route redirects to `dashboard`
- **`/dashboard`** = Default route for showing the **weather** in the 5 cities
- **`/dashboard/forecast/:id`** = Specific route to show the **forecast** of a given city
- `**` = Used for unknown address and redirects to a non-found component

### 2.3 Angular Modules

> The word 'artefact' refers to components, pipes, directives, services, etc.

- **App Module:** Default module for initialization
- **Dashboard Module:** Contains every artefact required to show the weather in **_the 5 cities_**
- **Core Module:** Contains singleton services.
- **Shared Module:** Contains interfaces and components used throughout the application.
- **Material Module:** Imports and exports all the material modules required in the application.
- **Forecast Module [Lazy Loaded/ Preloaded]:** Contains every artefact required to show the forecast of a **_given city_**. This module is preloaded, i.e. it is only loaded after the app has successfully rendered.

### 2.4 Features to be added in the future

- Cache requests for 10 minutes as per tips listed [here](https://openweathermap.org/appid#get)
- Add emoji pipe to show the emoji based on the weather
- Show tooltips the first time a user opens the app as if it were an interactive tutorial
